#include <iostream>
#include "floatList.h"
FloatList::ListNode* mergeLists(FloatList::ListNode* l1, FloatList::ListNode* l2);
FloatList::ListNode* removeDuplicatesInMergedLists(FloatList::ListNode* l3);

void printListReverse(FloatList::ListNode* node) {
	if (node == nullptr) {
		return;
	}
	printListReverse(node->next);
	std::cout << node->value << std::endl;
}

int main(void)
{
	FloatList list1;
	list1.appendNode(1);
	list1.appendNode(5);
	list1.appendNode(9);
	list1.appendNode(14);
	list1.appendNode(15);
	list1.appendNode(21);
	std::cout << "Printing list 1: " << std::endl;
	list1.displayList();

	FloatList list2;
	list2.appendNode(9);
	list2.appendNode(10);
	list2.appendNode(14);
	list2.appendNode(19);
	list2.appendNode(29);
	list2.appendNode(37);
	std::cout << "Printing list 2: " << std::endl;
	list2.displayList();

	std::cout << "Printing list 3: " << std::endl;

	FloatList::ListNode* merged = mergeLists(list1.head, list2.head);
	FloatList list3;
	list3.head = merged;
	list3.displayList();

	std::cout << "Printing list 4: " << std::endl;
	FloatList::ListNode* nodups = removeDuplicatesInMergedLists(merged);
	FloatList list4;
	list4.head = nodups;
	list4.displayList();
	std::cout << "Printing list 1 in reverse order: " << std::endl;
	printListReverse(list1.head);
	return 0;
}

FloatList::ListNode* mergeLists(FloatList::ListNode* l1, FloatList::ListNode* l2)
{
	FloatList::ListNode* mergedList = nullptr;
	FloatList::ListNode** current = &mergedList;
	while (l1 && l2) {
		if (l1->value <= l2->value) {
			*current = l2;
			l2 = l2->next;
		}
		current = &((*current)->next);
	}
	*current = l1 ? l1 : l2;
	return mergedList;
}

FloatList::ListNode* removeDuplicatesInMergedLists(FloatList::ListNode* l3)
{
	FloatList::ListNode* current = l3;
	while (current && current->next) {
		if (current->value == current->next->value) {
			FloatList::ListNode* temp = current->next;
			current->next = current->next->next;
			delete temp;
		}
		else {
			current = current->next;
		}
	}
	return l3;
}
