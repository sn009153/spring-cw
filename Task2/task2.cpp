#include "floatList.h"
#include <iostream>

// Append a node to the end of the list
void floatList::appendNode(float num) {
    ListNode* newNode = new ListNode;
    newNode->value = num;
    newNode->next = nullptr;

    if (head == nullptr) {
        head = newNode;
    }
    else {
        ListNode* current = head;
        while (current->next != nullptr) {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Display the contents of the list
void floatList::displayList() {
    ListNode* current = head;
    while (current != nullptr) {
        std::cout << current->value << std::endl;
        current = current->next;
    }
}

// Delete a node from the list
void floatList::deleteNode(float num) {
    if (head == nullptr) {
        return;
    }
    if (head->value == num) {
        ListNode* temp = head;
        head = head->next;
        delete temp;
        return;
    }
    ListNode* current = head;
    while (current->next != nullptr) {
        if (current->next->value == num) {
            ListNode* temp = current->next;
            current->next = current->next->next;
            delete temp;
            return;
        }
        current = current->next;
    }
}